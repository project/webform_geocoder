<?php

declare(strict_types=1);

namespace Drupal\webform_geocoder\Plugin\WebformElement;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geocoder\DumperInterface;
use Drupal\geocoder\DumperPluginManager;
use Drupal\geocoder\Entity\GeocoderProvider;
use Drupal\geocoder\GeocoderInterface;
use Drupal\geocoder\GeocoderProviderInterface;
use Drupal\geocoder\ProviderPluginManager;
use Drupal\webform\Annotation\WebformElement;
use Drupal\webform\Plugin\WebformElement\WebformComputedTwig;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'webform_computed_twig' element.
 *
 * @WebformElement(
 *   id = "webform_geocoder",
 *   label = @Translation("Geocoder"),
 *   description = @Translation("Provides an item to convert a twig resutl via geocoder."),
 *   category = @Translation("Computed Elements"),
 * )
 */
final class WebformGeocoder extends WebformComputedTwig {

  protected GeocoderInterface $geocoder;

  protected ProviderPluginManager $providerPluginManager;

  protected DumperPluginManager $dumperPluginManager;

  /**
   * @var array<\Drupal\geocoder\GeocoderProviderInterface>
   */
  protected array $configuredProviders;

  /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->providerPluginManager = $container->get('plugin.manager.geocoder.provider');
    $instance->dumperPluginManager = $container->get('plugin.manager.geocoder.dumper');
    return $instance;
  }

  protected function defineDefaultProperties() {
    return parent::defineDefaultProperties() + [
      'webform_geocoder_provider' => '',
      'webform_geocoder_dumper' => '',
    ];
  }

  public function form(array $form, FormStateInterface $form_state) {
    $element = parent::form($form, $form_state);

    // Add providers and dumper settings.
    $element['webform_geocoder'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Geocoder'),
      '#description' => $this->t('You must provide the data to geocode in the template.'),
    ];
    // Note: Default values are handled by webform.
    // @todo Port enabledProviders and multipleProviders from geocoder.
    /** @see \Drupal\geocoder_field\Plugin\Field\GeocodeFormatterBase::settingsForm */
    $element['webform_geocoder']['webform_geocoder_provider'] = [
      '#type' => 'select',
      '#title' => 'Provider',
      '#options' => $this->getConfiguredProvidersAsOptions(),
      '#description' => t('Set the geocoder provider.'),
    ];

    $element['webform_geocoder']['webform_geocoder_dumper'] = [
      '#type' => 'select',
      '#title' => 'Output format',
      '#options' => $this->dumperPluginManager->getPluginsAsOptions(),
      '#description' => t('Set the output format of the value. Ex, for a geofield, the format must be set to WKT.'),
    ];

    // Reorder.
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $element = ['element' => $element['element']]
      + ['webform_geocoder' => $element['webform_geocoder']]
      + $element;

    return $element;
  }

  public function computeValue(array $element, WebformSubmissionInterface $webform_submission) {
    $address = parent::computeValue($element, $webform_submission);
    if ($address) {
      $configuredProvider = $this->fetchConfiguredProvider($element['#webform_geocoder_provider']);
      $dumper = $this->fetchDumper($element['#webform_geocoder_dumper']);
      if ($configuredProvider && $dumper) {
        $geocoder = $configuredProvider->getPlugin();
        $addressCollection = $geocoder->geocode($address, [$configuredProvider]);
        // @todo Someday port geocoder_autocomplete to geocoder api, and offer
        //   as webform element.
        if ($addressCollection->count()) {
          $location = $addressCollection->first();
          /** @noinspection PhpUnnecessaryLocalVariableInspection */
          $geocoded = $dumper->dump($location);
          return $geocoded;
        }
      }
    }
    return '';
  }

  protected function fetchConfiguredProvider(string $configuredProviderId): ?GeocoderProviderInterface {
    $storage = $this->entityTypeManager->getStorage('geocoder_provider');
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $storage->load($configuredProviderId);
  }

  protected function fetchDumper(string $dumperId): ?DumperInterface {
    try {
      /** @noinspection PhpIncompatibleReturnTypeInspection */
      return $this->dumperPluginManager->createInstance($dumperId);
    }
    catch (PluginNotFoundException) {
      return NULL;
    }
  }

  protected function getConfiguredProvidersAsOptions(): array {
    return array_map(
      fn(GeocoderProvider $provider) => $provider->label(),
      $this->getConfiguredProviders()
    );
  }

  /**
   * @return \Drupal\geocoder\GeocoderProviderInterface[]
   */
  public function getConfiguredProviders(): array {
    if (!isset($this->configuredProviders)) {
      $storage = $this->entityTypeManager->getStorage('geocoder_provider');
      $this->configuredProviders = $storage->loadMultiple();
    }
    return $this->configuredProviders;
  }

}
