<?php

declare(strict_types=1);

namespace Drupal\webform_geocoder\Element;

use Drupal\Core\Render\Annotation\RenderElement;
use Drupal\webform\Element\WebformComputedTwig;

/**
 * @RenderElement("webform_geocoder")
 */
final class WebformGeocoder extends WebformComputedTwig {}
